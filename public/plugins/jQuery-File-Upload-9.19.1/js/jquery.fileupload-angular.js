/*
 * jQuery File Upload AngularJS Plugin
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2013, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * https://opensource.org/licenses/MIT
 */

/* jshint nomen:false */
/* global define, angular, require */

;(function (factory) {
    'use strict';
    if (typeof define === 'function' && define.amd) {
        // Register as an anonymous AMD module:
        define([
            'jquery',
            'angular',
            './jquery.fileupload-image',
            './jquery.fileupload-audio',
            './jquery.fileupload-video',
            './jquery.fileupload-validate'
        ], factory);
    } else if (typeof exports === 'object') {
        // Node/CommonJS:
        factory(
            require('jquery'),
            require('angular'),
            require('./jquery.fileupload-image'),
            require('./jquery.fileupload-audio'),
            require('./jquery.fileupload-video'),
            require('./jquery.fileupload-validate')
        );
    } else {
        factory();
    }
}(function () {
    'use strict';

    angular.module('blueimp.fileupload', [])

        // The fileUpload service provides configuration options
        // for the fileUpload directive and default handlers for
        // File Upload events:
        .provider('fileUpload', function () {
            var scopeEvalAsync = function (expression) {
                    var scope = angular.element(this)
                            .fileupload('option', 'scope');
                    // Schedule a new $digest cycle if not already inside of one
                    // and evaluate the given expression:
                    scope.$evalAsync(expression);
                },
                addFileMethods = function (scope, data) {
                    var files = data.files,
                        file = files[0];
                    angular.forEach(files, function (file, index) {
                        file._index = index;
                        file.$state = function () {
                            return data.state();
                        };
                        file.$processing = function () {
                            return data.processing();
                        };
                        file.$progress = function () {
                            return data.progress();
                        };
                        file.$response = function () {
                            return data.response();
                        };
                    });
                    file.$submit = function () {
                        if (!file.error) {
                            return data.submit();
                        }
                    };
                    file.$cancel = function () {
                        return data.abort();
                    };
                },
                $config;
            $config = this.defaults = {
                handleResponse: function (e, data) {
                    var files = data.result && data.result.files;
                    if (files) {
                        data.scope.replace(data.files, files);
                    } else if (data.errorThrown ||
                            data.textStatus === 'error') {
                        data.files[0].error = data.errorThrown ||
                            data.textStatus;
                    }
                },
                add: function (e, data) {
                    if (e.isDefaultPrevented()) {
                        return false;
                    }
                    var scope = data.scope,
                        filesCopy = [];
                    angular.forEach(data.files, function (file) {
                        filesCopy.push(file);
                    });
                    scope.$parent.$applyAsync(function () {
                        addFileMethods(scope, data);
                        var method = scope.option('prependFiles') ?
                                'unshift' : 'push';
                        Array.prototype[method].apply(scope.queue, data.files);
                    });
                    data.process(function () {
                        return scope.process(data);
                    }).always(function () {
                        scope.$parent.$applyAsync(function () {
                            addFileMethods(scope, data);
                            scope.replace(filesCopy, data.files);
                        });
                    }).then(function () {
                        if ((scope.option('autoUpload') ||
                                data.autoUpload) &&
                                data.autoUpload !== false) {
                            data.submit();
                        }
                    });
                },
                done: function (e, data) {
                    if (e.isDefaultPrevented()) {
                        return false;
                    }
                    var that = this;
                    data.scope.$apply(function () {
                        data.handleResponse.call(that, e, data);
                    });
                },
                fail: function (e, data) {
                    if (e.isDefaultPrevented()) {
                        return false;
                    }
                    var that = this,
                        scope = data.scope;
                    if (data.errorThrown === 'abort') {
                        scope.clear(data.files);
                        return;
                    }
                    scope.$apply(function () {
                        data.handleResponse.call(that, e, data);
                    });
                },
                stop: scopeEvalAsync,
                processstart: scopeEvalAsync,
                processstop: scopeEvalAsync,
                getNumberOfFiles: function () {
                    var scope = this.scope;
                    return scope.queue.length - scope.processing();
                },
                dataType: 'json',
                autoUpload: false
            };
            this.$get = [
                function () {
                    return {
                        defaults: $config
                    };
                }
            ];
        })

        // Format byte numbers to readable presentations:
        .provider('formatFileSizeFilter', function () {
            var $config = {
                // Byte units following the IEC format
                // http://en.wikipedia.org/wiki/Kilobyte
                units: [
                    {size: 1000000000, suffix: ' GB'},
                    {size: 1000000, suffix: ' MB'},
                    {size: 1000, suffix: ' KB'}
                ]
            };
            this.defaults = $config;
            this.$get = function () {
                return function (bytes) {
                    if (!angular.isNumber(bytes)) {
                        return '';
                    }
                    var unit = true,
                        i = 0,
                        prefix,
                        suffix;
                    while (unit) {
                        unit = $config.units[i];
                        prefix = unit.prefix || '';
                        suffix = unit.suffix || '';
                        if (i === $config.units.length - 1 || bytes >= unit.size) {
                            return prefix + (bytes / unit.size).toFixed(2) + suffix;
                        }
                        i += 1;
                    }
                };
            };
        })

        // The FileUploadController initializes the fileupload widget and
        // provides scope methods to control the File Upload functionality:
        .controller('FileUploadController', [
            '$scope', '$element', '$attrs', '$window', 'fileUpload','$q',
            function ($scope, $element, $attrs, $window, fileUpload, $q) {
                var uploadMethods = {
                    progress: function () {
                        return $element.fileupload('progress');
                    },
                    active: function () {
       _�exist>s���A��cr itical_s@ection�-c ontext_unb�j�alan�ced� �-�D$��self�%�
�;@�rt_inv alid_arg�umVa�T�=c�0A�bad_t@ew���P�cas t@std@@A1��PBQ���__ non_rtti�_obj�p
�ԃ�_TaskColl�xBase@9�il��@�a_Re�FrantPPLL��Holde"r�PAV�@1�2@���2o�af��B�Ting+�da`W�aer�_�#Non_$�A$���$�
:��
���%�KJw�Stru�ctured?�0�SžPoli��2Ń��ABV01"OCb�A@IZB� �|60L` $` ��50�` �` 
\` (` �40��` d` 4`  ` ��30x` T` 
4` ` �20��` �` T` (` ��10�` �` *`` 0` ` �0�0�` �` X` �$` �/0�` ��` X` 0`  ` ��.�` h` <` �` �-`` d` �0` �,0�` *d` ,`  ` �+�0�` �` L` �` �*`` �` �X` (` �)�` *|` L`  ` �(�0�` �` x` 
<`  ` �'0��` h` H` ` �` �&�` �` 
p` <` �%0��0 t0 D0 0 ��$p0 �0 l0 �(0 �#0�0 
X0 (0 �"0��0 �0 �0 `0 �40 �!0�0 �`0 0 � 00 ��0 d0 D0 0 ���0 l0 @0 � 0 ��0 00 � 0 ��	0 80 ��00 h0  0 ��� 0 |0 <0 ��0�0 �0 �L0 0 ��0 *t0 D0  0 �+00 l0 ,0 ���	0 �0 @0 0 ��0�0 h0 �,0 ��
0 h0 
40 0 �0��0 �0 |0 P0 �,0 0 ��0 ��0 �0 �0 @0 �0 ��0 p0 *L0 ,0 0 ���0 �0 \0 ,0 ��0�0 �0 ��0 p0 L0 D0 <0 MSVCR7 1 RSDS�v �A���C������M�]�b msvcr110 d.pdb j��P�U�� � P1��u h�90�
p�3t�M �4���6P�� ��3�����]� 3��  p�u� ]á �u`j @�����a��������g����N����5���������� ���.��
�$��.с�(�	�.���,�
�.���0��.���4��.m��8��.T��<��.;��@��."��D��.	��H��\����L�\�ׁ�P�\辁�T�\襁�X�\茁�\�\�s��`�\�Z��d�\�A��h�\�(��l�\���p�������t���݁�x���ā�|��諁����蒁���� �y�����!�`�����"�G�����#�.�����$�����p%������q�&����q�'�ʁ��q�(豁��q�)蘁��q�*����q�+�f���q�,�M���q�-�4���q�.����q�/�����0��� ��Ȇ1�Ё��̆2跁��І3螁��Ԇ4腁��؆5�l���܆6�S�����7�:����8�!����9����:�߸ ������P1@a��� p �u`j;����`�`<�`@`�`=�0 0�0>苇0 0 Q1Ƹ?�r0�@�Y�A�@�B�'�C�pD��� �qE��qF�� qG�$qH�(qI�x,qJ�_0qK�F4qL�-8qM�<�N����|�@O���DP���HQ��LR��PS�~�TT�e�XU�L�\V�3�`W��dX��hY����D�lZ���p[��t\��x]��|^�k��_�R��`�9��a� ��b���c��� ��d����e���f���g���h�q��i�X��j�?��k�&��l��m��� �n���o���p��q��r�w�s�^�t�E�u�,�v��w��.� �x�.��y�.��z�.��{�.��|�.}�}�.d�~�NK R��\�2 `h�   �Waaaf�a����aaf�a��aaaf�a��aaaf�a��aaaf�a��aaaf�a�naaa f�a�Raaa$f�a�6aaa(f�a�aaa,f�a\��`aa0f�]��1�4��]�Ʊ1�8��]���1�<��]���1�@��]�r�1�D��]�V�1�H��]�:�1�L��]��1�P��]��1�T������p1�X����ʱ1�\������1�`������1�d����v�1�h����Z�1�l����>�1�p����"�1�t�����1�x�r����1�|u���α1��u�����1��u�����1��u���z�1��u���^�1��u���B�1��u���&�1��u���
�1�������1�꠶��ұ1�ꤶ����1�ꨶ����1�ꬶ��~�1�갶��b�1�괶��F�1�긶��*�1�꼶���1���������1ձĶ��ֱ1ձȶ����1ձ̶����1ձж����1ձԶ��f�1ձض��J�1ձܶ��.�1ձ����1��������1����ڱ�1�������1�������1��������1�����j��1�����N��1� S�o0n���2�1������1����\��1���]�ޱ1���]�±1������1���̬�N@$ � � U� ���0 �0 �0 �� Exit Process �Ge� Addr bLoa dLibrary A  pSet Unhandle dExcepti onFilter  KERNEL3 2.dll���T*Y� ��k� I  1 O   <h  `@�I:�  b0 {0 �0 ��0 �0 �0 �0 �;  *0 C0 �\0 u0 �0 �0 �'�o�p �0 s� 
�0 � <  ��� 9�o$� =0 �V0 o0 �0 �0 *�0 �0 �0 =�  {��0 @q�C0 070 P0 �i0 �0 �0 �0 *�0 �0 �0 >�  10 J0 c0 �|0 �0 �0 �0 
�0 �0 _��?  + 0D 0�] 0v � � *� � � @�  % > W �p � � � 
� � A  � 8 Q j *� � � ���  � � � � 7�   B�  l �  �2 K � d �} � � � �� L�  � �� 1�  n ���  � �  '�  X ;�  v m�  *� ;� � C�  ,�E�^��w�����
ۀ�D  �&�?�X�q��������Հ��E   ��9�R�k��������π��F  �3��L�e�~���*��ɀ�G�  �9�U��q�����ŀ
���H  �5�Q�m������  � T ��݀��PI  1�M�iU��� �� �� �� �� J  -U� I� e� {��A� /�  o� �U� ���� �� �� �� K  )U� E� a� ��	$P�  }���2P�  �����A� �  f� �A� �  �� �U������� �`�  	L��� AU� ]� ��y�9P�  ���� ���� �  PU� �� ���� �A�M  !� =U� ��Y�u� ��� �� �� 5��#U� �� �� �� �� �� �  (U� C� ^� z� �U� �� �� �� V��k� ?� c� �� �� �� !� T E� `� }� �� �� �� � T :� X� u� �� �� �� %� T S� �� �� �A� #�  R� �� �� �� � T 8� t� �� �A� ,�  V� |U� �� �� �� V��i� Z� �` �` �` �  UU` �` �` �` �Y`  �`` p` �` �` �` <� T Z` x` �` �` �` �  $U` N` y` �` �` �` �` � T 3` M` h` �U` �` �` �` 	V��` ]` �` �e` �` ��R` UU` }` �` �` �A` %�  M` uU` �` �` �` V�`+` Y` v` ��` �` �` ��U` D` f` �` ��` �` �` ��NU` E` d` �` ��` �` �` ��(U` K` j` �` �e` �` ��` SU` z` �` �` �` �` �  >U` [` w` �` �` �` �` � T K` h` �` ��` �` �`  �`QU` L` b` {` �U` �` �` �` �A�Y%�  [` ��` �` �` �`�U` C` \` u` �U` �` �` �` �Y` ��` :` RU` l` �` �` �Y` �`�` ;` RU` o` �` �` �U` �` �` �` P�  .` F` \U` r` �` �` �` �` �` � T 4` Q` p` �U` �` �` �` �A` �  =` XU` n` �` �` �e` �` �`�` =U` Y` t` �` �U` �` �` �` V��e` Y` z` �U` �` �` �` V�`` Q` p` �U` �` �` �` �` �` �  2U` W` w` �` �U` �` �` �` #V�`x` y` �` �U` �` �` �` V���` 0` E` cU` �` �` �` �e` �` �`.` GU` a` |` �` ��` �` �` ��#U` a` �` �` �e` �` �`:` 8U` P` g` �0 �U0 �0 �0 �0 V��d0 U0 v0 �U0 �0 �0 �0 P�  *0 D0 aU0 �0 �0 �0 �e0 �0 �0<0 OU0 p0 �0 �0 �0 �0 �  #U0 ;0 R0 m0 �U0 �0 �0 �0 V۰*0 M0 l0 �U0 �0 �0 �0 
V��0 K0 h0 �0 �0 �0  � T F0 r0 �0 �0 �0 �  <U0 `0 �0 �0 ��0 �0 �0 �0=U0 I0 m0 �0 �0 �0 �0 
� T "0 90 T0 mU0 �0 �0 �0 �e0 �0 �,0 UU0 r0 �0 �0 ȕ0 �0 �0 ��LU0 H0 `0 v0 �U0 �0 �0 �0 �Y0 �0	0 A0 SU0 f0 �0 �0 N  �p � T 0 90 S0 mU0 �0 �0 �0 �0 �0 �  ;U0 \0 }0 �0 �e0 �0 ��N0 FU0 e0 �0 �0 �0 �0 �  /U0 O0 p0 �0 �0 �0 �0 � T 60 W0 x0 �U0 �0 �0 �0 V�0 Z0 y0 �U0 �0 �0 �0 V��s0 ;0 \0 }U0 �0 �0 �0 V�0 C0 d0 �U0 �0 �0 �0 V�0W0 K0 l0 �U0 �0 �0 �0 �Y0 ��-0 T0 rU0 �0 �0 �0 �Y0 �00 J0 iU0 �0 �0 �0 �e0 �0 �0F0 3U0 B0 Q0 c0 xU0 �0 �0 �0 �Y0 ��A0 U0 xU0 �0 �0 �0 �e0 �0 �00 :U0 U0 n0 �0 �U0 �0 �0 �0 �Y0 ��0 N0 kU0 �0 �0 �0 �e0 �0 ��90 JU0 m0 �0 �0 �e0 �0 �p0 DU0 c0 �0 �0 Õ0 �0 �0  ��iU0 W0 q0 �0 �U0 �0 �0 �0 V�0R0 H0 c0 U0 �0 �0 �0 �Y0 ��0 ;0 WU0 t0 �0 �0 �0 �0 �0 � T '0 D0 a0 ~U0 �0 �0 �0 �Y0 �050 R0 qU0 �0 �0 �0 �Y0 ��0 K0 lU0 �0 �0 �0 �Y0 �0�0 >0 ]U0 {0 �0 �0 �e0 �0 �0�0 ?U0 \0 {0 �0 ��0 �0 �0 �p;U0 J0 e0 �0 �U0 �0 �0 �0 V�00 E0 `0 {U0 �0 �0 �0 �e0 �0 �pR0 LU0 f0 �0 �0 �0 �0 �0  T 0 =0 ^0 }U0 �0 �0 �0 �A0  '0 DU0 a0 {0 �0 �0 �0 �0 T 0 50 P0 kU0 �0 �0 �0 �0 �0  U0 .0 D0 [0 rU0 �0 �0 �0 �e0 �0 
p0 ?U0 [0 x0 �0 �U0 �0 �0 �0 P %0 ?0 ]U0 |0 �0 �0 �0 �0 �0 T #0 90 P0 kU0 �0 �0 �0 �Y0 00 D0 gU0 �0 �0 �0  P ,0 X0 �U0 �0 �0 �0 
P	 ;0 z0 �0 �0 
 AU0 ^0 v0 �0 �U0 �0 �0 �0 P 40 U0 vU0 �0 �0 �0 �A0  +0 JU0 i0 �0 �0 �0 �0 	 *U0 F0 ]0 u0 �U0 �0 �0 �0 �A0 
 $0 @U0 _0 ~0 �0 �0 �0 �0 T 10 K0 e0 U0 �0 �0 �0 �Y0 
p0 =0 ZU0 y0 �0 �0 �0 �0  0 k��8 U 0r 0�� 0� � � � # > �X o � � *� � � � . J i �� � � � ��  ) �@ ] ~ � 
� �  �" C a x �� � � � �� 	 $ �? Z x � �� � � � � 0 I �` w � � *� � � � A�b��������؀�� *�D��a��������[�C��f�����΀�� 1��S�t�����
݀�� �:�Y�x���*��ހ��"� G�i���*��΀�� 4�U�x�*����߀ ��݀C�b���*��ɀ� � 3�R� s� ��� �� �� �� �!�Y� <� S� �j� �� �� �� 
�� �� 
" �&� A� ]� z� ��� �� �� �� �#  � =� �Z� x� �� �� 
�� ��  $ �� 4� O� l� ��� �� �� �� �% %� ?� �X� s� �� �� 
�� �� & �!� >� [� v� ��� �� �� �� ��� '��� A� �Z� t� �� �� 
�� �� ( �C� g� �� �� 
�� �� ) �� ;� V� q� ��� �� �� �� �*�+� A� Y� �t� @�  ����� �� �� �� �+ 7� T� �o� �� �� �� ��� �� ,�� �F� g� V������ �� �� �� �
-�n� ?� [� �y� �� �� �� ��� .�?� F` �c` �` �` �` 
�` �` / �` -` B` [` �t` �` �` �` 
�` �`  0 �` ` /` @` ��D S� l` ��`E �`��` �` �` �` ��` �` 1�` �7` L` g` ` ���� �` �` ��` �` �` �` �
2`1` 3` H` �]` q` �` �` ��` �` �` �` �3 ` -` �A` V` j` ` ��` �` �` �` ��` 4� ` -` �B` X` s` �` ��` �` �` �` ��` 5 '` �:` N` d` ~` ��` �` �` �` ��` 6`` >` �U` l` �` �` ��` �` �` �` �7`�` J` c` �|` �` �