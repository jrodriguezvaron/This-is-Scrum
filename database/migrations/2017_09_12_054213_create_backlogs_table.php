<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBacklogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('backlogs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('description');
            $table->binary('file')->nullable();
            
            $table->bigInteger('fk_project_id')->unsigned();
            $table->foreign('fk_project_id')
            ->references('id')
            ->on('projects')
            ->onDelete('cascade');

            $table->bigInteger('fk_type_id')->unsigned();
            $table->foreign('fk_type_id')
            ->references('id')
            ->on('type_requests')
            ->onDelete('cascade');

            $table->bigInteger('fk_priority_id')->unsigned();
            $table->foreign('fk_priority_id')
            ->references('id')
            ->on('priority_requests')
            ->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('backlogs');
    }
}
