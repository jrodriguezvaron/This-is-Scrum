<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSprintsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sprints', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('description');
            $table->date('initial_date');
            $table->date('final_date');

            $table->bigInteger('fk_state_id')->unsigned()->default(1);
            $table->foreign('fk_state_id')
            ->references('id')
            ->on('states')
            ->onDelete('cascade');

            $table->bigInteger('fk_project_id')->unsigned();
            $table->foreign('fk_project_id')
            ->references('id')
            ->on('projects')
            ->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('history_sprint', function(Blueprint $table){
            $table->bigIncrements('id');
            $table->bigInteger('history_id')->unsigned();
            $table->bigInteger('sprint_id')->unsigned();

            $table->foreign('history_id')
            ->references('id')
            ->on('histories');

            $table->foreign('sprint_id')
            ->references('id')
            ->on('sprints');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sprints');
    }
}
