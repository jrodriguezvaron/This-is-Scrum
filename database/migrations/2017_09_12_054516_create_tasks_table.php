<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->binary('file')->nullable();
            $table->string('description');

            $table->bigInteger('fk_phase_id')->unsigned();
            $table->foreign('fk_phase_id')
            ->references('id')
            ->on('phase_tasks')
            ->onDelete('cascade');

            $table->bigInteger('fk_priority_id')->unsigned();
            $table->foreign('fk_priority_id')
            ->references('id')
            ->on('priority_tasks')
            ->onDelete('cascade');

            $table->bigInteger('fk_project_id')->unsigned();
            $table->foreign('fk_project_id')
            ->references('id')
            ->on('projects')
            ->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('user_sprint_task', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('user_id')->unsigned();
            $table->bigInteger('sprint_id')->unsigned();
            $table->bigInteger('task_id')->unsigned();


            $table->foreign('task_id')
            ->references('id')
            ->on('tasks')
            ->onDelete('cascade');

            $table->foreign('sprint_id')
            ->references('id')
            ->on('sprints')
            ->onDelete('cascade');
            
            $table->foreign('user_id')
            ->references('id')
            ->on('user_rol_projects')
            ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tasks');
    }
}
