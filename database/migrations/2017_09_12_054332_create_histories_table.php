<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('histories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('objective');
            $table->bigInteger('fk_phase_id')->unsigned();
            $table->foreign('fk_phase_id')
            ->references('id')
            ->on('phase_histories')
            ->onDelete('cascade');

            $table->bigInteger('fk_priority_id')->unsigned();
            $table->foreign('fk_priority_id')
            ->references('id')
            ->on('priority_histories')
            ->onDelete('cascade');

            $table->bigInteger('fk_project_id')->unsigned();
            $table->foreign('fk_project_id')
            ->references('id')
            ->on('projects')
            ->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('backlog_history', function(Blueprint $table){
            $table->bigIncrements('id');
            $table->bigInteger('backlog_id')->unsigned();
            $table->bigInteger('histoy_id')->unsigned();

            $table->foreign('backlog_id')
            ->references('id')
            ->on('backlogs');

            $table->foreign('history_id')
            ->references('id')
            ->on('histories');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('histories');
    }
}
