<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('last_name');
            $table->string('name_user')->unique();
            $table->string('email')->unique();
            $table->string('password');
            $table->bigInteger('fk_state_id')->unsigned()->default(2);
            $table->bigInteger('fk_profile_id')->unsigned()->default(2);
            $table->foreign('fk_state_id')->references('id')->on('states')->onDelete('cascade');
            $table->foreign('fk_profile_id')->references('id')->on('profile_users')->onDelete('cascade');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
