<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $primaryKey = 'id';
    protected $fillable = [
        'name','last_name','name_user','email', 'password','fk_state_id','fk_profile_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];



    public function rol()
    {
        return $this->belongsTo('App\Models\Profile_User','fk_profile_id','id');
    }

    public function state()
    {
        return $this->belongsTo('App\Models\State','fk_state_id','id');
    }
    
    public function user_rol_projects(){
         return $this->hasMany('App\Models\User_Rol_Project','fk_user_id','id');
    }

    public function admin(){
        return $this->fk_profile_id == '1';
    }

    public function id_user(){
        return $this->id;
    }
}
