<?php

namespace App\Http\Controllers\Task;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Phase_Task;
use App\Models\Priority_Task;
use App\Models\Task;
use App\Models\Project;
use Laracasts\Flash\Flash;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        flash()->overlay('Tú no has podido acceder por URL, por favor inicia sesión o registrate', 'Error!!!');
        $this->middleware('auth');
        $this->middleware('revalidate');

    }
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $tasks = new Task($request->all());   //almaceno todos los datos recibidos de la vista dentro de la variable  $backlog
        $tasks->save();  //guardo todo lo que contenga la variable backlog

        Flash::success('Se ha registrado la peticion "' . $tasks->name . '" exitosamente');  //creo un mensaje para la vista... posteriormente la vista recibira este contenido
        return redirect()->route('Tasks.shows', $request->fk_project_id);    //Redirecciono a la ruta donde consulto de manra especifica los backlogs dento de un proyecto
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        
        $tasks = Task::find($id); //busco el backlog que provenga de la vista mediante el $id que solicito en el formulario de la vista
        $type = Phase_Task::orderBy('name','ASC')->pluck('name', 'id')->toArray();      //consulto de manera general y ordeno de forma ascendiente lqs fases que tiene una tarea
        $priority = Priority_Task::orderBy('id','ASC')->pluck('name', 'id')->toArray();    //consulto de manera general y ordeno de forma ascendiente las prioridades de una tarea
        $project = Project::find($tasks->fk_project_id);  //busco el proyecto que provenga de la vista mediante el $id que solicito en el formulario de la vista
        $phase = Phase_Task::all();


        return view('Admin.Task.edit')   //Retorno a la vista 
        ->with('tasks', $tasks)       //con todo lo que contenga la variable $backlogs
        ->with('type', $type)               //con todo lo que contenga la variable $type
        ->with('priority', $priority) 
        ->with('phase', $phase)      //con todo lo que contenga la variable $priority
        ->with('project', $project);        //con todo lo que contenga la variable $project
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            if($request->ajax()){
                
                return "AJAX";
            
            }else{

        $tasks = Task::find($id);     //busco el backlog que provenga de la vista mediante el $id que solicito en el formulario de la vista
        $tasks->fill($request->all());   //almaceno todos los datos recibidos de la vista dentro de la variable  $backlogs
        
        $tasks->save();                  //guardo todo lo que contenga la variable $backlogs
        $project = Project::find($tasks->fk_project_id);    // busco con el atriburo fk_project_id de la variable $backlog entre los proyectos exitentes


        flash('Se ha modificado la tarea "' . $tasks->name . '" exitosamente')->important();  //creo un mensaje para la vista... posteriormente la vista recibira este contenido
        return redirect()->route('Tasks.shows', $tasks->fk_project_id);   //Redirecciono a la ruta donde consulto de manra especifica los backlogs dento de un proyecto
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function index_show($id)
    {
        $type = Phase_Task::orderBy('name','ASC')->pluck('name', 'id')->toArray();      //consulto de manera general y ordeno de forma ascendiente lqs fases que tiene una tarea
        $priority = Priority_Task::orderBy('id','ASC')->pluck('name', 'id')->toArray();    //consulto de manera general y ordeno de forma ascendiente las prioridades de una tarea
        $project = Project::find($id);  //busco el proyecto que provenga de la vista mediante el $id que solicito en el formulario de la vista
        $phase = Phase_Task::with('tasks')->get();
        

        $tasks = Task::where('fk_project_id', $id)->orderBy('fk_priority_id','ASC')->get();    //codigo para consultar todos los backlogs que le pertenecen a un proyecto

        return View('Admin.Task.tasks')   //retorno a la vista de tasks
        ->with('tasks', $tasks)   //con todo lo que contenga la variable $backlogs 
        ->with('project', $project)     //con todo lo que contenga la variable $project
        ->with('type', $type)           //con todo lo que contenga la variable $type
        ->with('priority', $priority)  //con todo lo que contenga la variable $priority
        ->with('phase', $phase);
    }
}
