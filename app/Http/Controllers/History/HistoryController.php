<?php

namespace App\Http\Controllers\History;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\History;
use App\Models\Project;
use App\Models\Phase_History;
use App\Models\Backlog;
use App\Models\Priority_History;
use Laracasts\Flash\Flash;

class HistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        flash()->overlay('Tú no has podido acceder por URL, por favor inicia sesión o registrate', 'Error!!!');
        $this->middleware('auth');
        $this->middleware('revalidate');

    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $history = new History($request->all());
        $history->save();
        
        $backlog = Backlog::all();         

        $history->backlogs()->sync($request->backlog);

        Flash::success('Se ha registrado la peticion "' . $history->objective . '" exitosamente');
        return redirect()->route('Historys.shows', $request->fk_project_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $history = History::find($id);
        $phase = Phase_History::orderBy('id','ASC')->pluck('name', 'id')->toArray();
        $priority = Priority_History::orderBy('id','ASC')->pluck('name', 'id')->toArray();
        $project = Project::find($history->fk_project_id);


        return view('Admin.History.edit')
        ->with('history', $history)
        ->with('phase', $phase)
        ->with('priority', $priority)
        ->with('project', $project);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        $history = History::find($id);
        $history->fill($request->all());
        $history->save();

        $backlog = Backlog::all();
        $history->backlogs()->sync($request->backlog);

        flash('Se ha modificado la historia "' . $history->objective . '" exitosamente')->important();
        return redirect()->route('Historys.shows', $history->fk_project_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function index_show($id)
    {
    
        $phase = Phase_History::orderBy('id','ASC')->pluck('name', 'id')->toArray();
        $priority = Priority_History::orderBy('id','ASC')->pluck('name', 'id')->toArray();
        $project = Project::find($id);
        $backlog = Backlog::with("backlogs")->where('fk_project_id', $project->id)->orderBy('fk_priority_id','ASC')->pluck('name', 'id')->toArray();
        $history =  History::where('fk_project_id', $project->id)->orderBy('fk_priority_id','ASC')->get();

        return View('Admin.History.historys')   //retorno a la vista de backlog
        ->with('phase', $phase)
        ->with('priority', $priority)
        ->with('project', $project)
        ->with('history', $history)
        ->with('backlog', $backlog);   //retorno a la vista --CON-- la consulta delos backlogs que le pertenecen a un proyecto
    }
}
