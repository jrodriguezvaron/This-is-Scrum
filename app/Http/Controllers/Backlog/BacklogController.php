<?php

namespace App\Http\Controllers\Backlog;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Backlog;
use App\Models\Project;
use App\Models\Type_Request;
use App\Models\Priority_Request;
use Laracasts\Flash\Flash;
/* Importo o uso los modelos o plugins necesarios para aplicarlos en el controlador mediante sus namespaces*/

class BacklogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        flash()->overlay('Tú no has podido acceder por URL, por favor inicia sesión o registrate', 'Error!!!');
        $this->middleware('auth');
        $this->middleware('revalidate');
    }
    
    public function index()
    {/*
        /*  //codigo para llamada masiva del backlog
        $backlogs = Backlog::with('project')->get();
        //llamo la vista que se va a cargar
        return View('Admin.Backlog.backlogs')->with('backlogs', $backlogs);
        */
       /*
            No uso por el momento este metodo ya que no me sirve para buscar de forma especifica los backlogs correspondientes a un proyecto
        */
       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //no uso este metodo ya que la ventana modal me facilita el uso de este...
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $backlogs = new Backlog($request->all());   //almaceno todos los datos recibidos de la vista dentro de la variable  $backlog
        $backlogs->save();  //guardo todo lo que contenga la variable backlog

        Flash::success('Se ha registrado la peticion "' . $backlogs->name . '" exitosamente');  //creo un mensaje para la vista... posteriormente la vista recibira este contenido
        return redirect()->route('Backlogs.shows', $request->fk_project_id);    //Redirecciono a la ruta donde consulto de manra especifica los backlogs dento de un proyecto

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $backlogs = Backlog::find($id); //busco el backlog que provenga de la vista mediante el $id que solicito en el formulario de la vista
        $type = Type_Request::orderBy('name','ASC')->pluck('name', 'id')->toArray();    //consulto de manera general y ordeno de forma ascendiente los tipos de requisitos que necesita un backlog
        $priority = Priority_Request::orderBy('name','ASC')->pluck('name', 'id')->toArray();    //consulto de manera general y ordeno de forma ascendiente las prioridades que necesita un backlog
        $project = Project::find($backlogs->fk_project_id); // busco con el atriburo fk_project_id de la variable $backlog entre los proyectos exitentes
        return view('Admin.Backlog.edit')   //Retorno a la vista 
        ->with('backlogs', $backlogs)       //con todo lo que contenga la variable $backlogs
        ->with('type', $type)               //con todo lo que contenga la variable $type
        ->with('priority', $priority)       //con todo lo que contenga la variable $priority
        ->with('project', $project);        //con todo lo que contenga la variable $project

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $backlogs = Backlog::find($id);     //busco el backlog que provenga de la vista mediante el $id que solicito en el formulario de la vista
        $backlogs->fill($request->all());   //almaceno todos los datos recibidos de la vista dentro de la variable  $backlogs
        $backlogs->save();                  //guardo todo lo que contenga la variable $backlogs
        $project = Project::find($backlogs->fk_project_id);    // busco con el atriburo fk_project_id de la variable $backlog entre los proyectos exitentes


        flash('Se ha modificado la peticion "' . $backlogs->name . '" exitosamente')->important();  //creo un mensaje para la vista... posteriormente la vista recibira este contenido
        return redirect()->route('Backlogs.shows', $backlogs->fk_project_id);   //Redirecciono a la ruta donde consulto de manra especifica los backlogs dento de un proyecto
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

     public function index_show($id)
    {
        
        $type = Type_Request::orderBy('name','ASC')->pluck('name', 'id')->toArray();          //consulto de manera general y ordeno de forma ascendiente los tipos de requisitos que necesita un backlog
        $priority = Priority_Request::orderBy('id','ASC')->pluck('name', 'id')->toArray();    //consulto de manera general y ordeno de forma ascendiente las prioridades que necesita un backlog
        $project = Project::find($id);  //busco el proyecto que provenga de la vista mediante el $id que solicito en el formulario de la vista
        $backlogs = Backlog::where('fk_project_id', $id)->orderBy('fk_priority_id','DESC')->get();    //codigo para consultar todos los backlogs que le pertenecen a un proyecto
        

        return View('Admin.Backlog.backlogs')   //retorno a la vista de backlog
        ->with('backlogs', $backlogs)   //con todo lo que contenga la variable $backlogs 
        ->with('project', $project)     //con todo lo que contenga la variable $project
        ->with('type', $type)           //con todo lo que contenga la variable $type
        ->with('priority', $priority);  //con todo lo que contenga la variable $priority
    }
}
