<?php

namespace App\Http\Controllers\Sprint_history;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\History;
use App\Models\Sprint;
use App\Models\Histori_Sprint;

class Sprint_historyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        flash()->overlay('Tú no has podido acceder por URL, por favor inicia sesión o registrate', 'Error!!!');
        $this->middleware('auth');
        $this->middleware('revalidate');
    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sprint_history = new Histori_Sprint($request->all());
        $sprint_history->save();
        
        $history = History::where('history_id', $sprint_history->history_id)->get();

        
        return redirect()->route('Historys.shows', $request->history_id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $sprint_history = Histori_Sprint::find($id);
        $sprint_history->fill($request->all());
        $sprint_history->save();

        
        $history = History::where('history_id', $sprint_history->history_id)->get();

        
        return redirect()->route('Historys.shows', $request->history_id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
