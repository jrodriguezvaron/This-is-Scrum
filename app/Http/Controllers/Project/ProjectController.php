<?php

namespace App\Http\Controllers\Project;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Project;
use App\Models\State;
use App\Models\User_Rol_Project;
use App\Models\Rol_User;
use Laracasts\Flash\Flash;
use App\User;


class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        flash()->overlay('Tú no has podido acceder por URL, por favor inicia sesión o registrate', 'Error!!!');
        $this->middleware('auth');
        $this->middleware('revalidate');

    }
    public function index()
    {
        
        $projects = Project::orderBy('id','ASC')->paginate();

        return view('Admin.Project.project')->with('projects', $projects); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user_project = new Project($request->all());
        $user_project->save();
        
        $rol_project =  new User_Rol_Project();
        $rol_project->fk_user_id = $request->algo;
        $rol_project->project()->associate($user_project);
        $rol_project->save();

        Flash::success('Se ha registrado el proyecto "' . $user_project->name . '" exitosamente');
        return redirect()->route('Projects.shows', $request->algo );

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return $id;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user_project = new Project($request->all());
        $user_project->save();

        
        Flash::success('Se ha actualizado el proyecto "' . $user_project->name . '" exitosamente');
        return redirect()->route('Projects.shows', $request->algo);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function index_show($id)
    {

        $user_project = User_Rol_Project::where('fk_user_id', $id)->orderBy('id','ASC')->get();
        $rol = Rol_User::all();
        


        return view('Admin.Project.project')
        ->with('user_project', $user_project)
        ->with('rol', $rol);
    }
}



