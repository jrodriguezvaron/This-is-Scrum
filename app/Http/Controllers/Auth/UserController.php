<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\State;
use App\Models\Profile_User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        flash()->overlay('Tú no has podido acceder por URL, por favor inicia sesión o registrate', 'Error!!!');
        $this->middleware('auth');
        $this->middleware('revalidate');

    }
    
    public function index()
    {
        $states = State::orderBy('name','ASC')->pluck('name', 'id')->toArray();
        $profiles = Profile_User::orderBy('name', 'ASC')->pluck('name', 'id')->toArray();
        $users = User::orderBy('id','ASC')->paginate();
        
        return view('auth.users')->with('user', $users)
        ->with('states', $states)
        ->with('profiles', $profiles);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $states = states::orderBy('name','ASC')->pluck('name', 'id_state')->toArray();
        $profiles = Profiles::orderBy('name', 'ASC')->pluck('name', 'id_profile')->toArray();
        
        return view('auth.edit')
        ->with('users', $user)
        ->with('states', $states)
        ->with('profiles', $profiles);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->fill($request->all());
        $user->save();

        flash('Se ha actualizado el usuario "' . $user->name ." ". $user->last_name . '" exitosamente')->important();
        return redirect()->route('User.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
