<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class History extends Model
{
    protected $table ="histories";
    protected $fillable = ['objective','fk_phase_id','fk_priority_id','fk_project_id'];
    protected $guarded = ["id"];


    public function project()
    {
        return $this->belongsTo('App\Models\Project','fk_project_id','id');
    }
    
    public function priority_history(){
        return $this->belongsTo('App\Models\Priority_History','fk_priority_id','id');
    }

    public function phase_history(){
        return $this->belongsTo('App\Models\Phase_History','fk_phase_id','id');
    }

    public function sprint_histories(){
         return $this->hasMany('App\Models\Sprint_History','fk_history_id','id');
    }

    public function backlogs()
    {
        return $this->belongsToMany('App\Models\Backlog')->withTimestamps();
    }
}
