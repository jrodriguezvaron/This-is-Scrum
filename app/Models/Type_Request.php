<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Type_Request extends Model
{
    protected $table ="type_requests";
    protected $fillable = ['name'];
    protected $guarded = ["id"];


    
    public function backlogs(){
         return $this->hasMany('App\Models\Backlog','fk_type_id','id');
    }
}
