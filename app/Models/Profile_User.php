<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Profile_User extends Model
{
   protected $table ="profile_users";
   protected $fillable = ["name"];
   protected $guarded = ["id"];
   


   public function users(){
         return $this->hasMany('App\User','fk_profile_id','id');
    }
}
