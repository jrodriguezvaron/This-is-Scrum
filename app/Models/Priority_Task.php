<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Priority_Task extends Model
{
    protected $table ="priority_tasks";
    protected $fillable = ['name'];
    protected $guarded = ["id"];

    public function tasks(){
         return $this->hasMany('App\Models\Task','fk_priority_id','id');
    }
}
