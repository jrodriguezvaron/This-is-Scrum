<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sprint extends Model
{
    protected $table ="sprints";
    protected $fillable = ['description','initial_date','final_date','fk_state_id','fk_project_id'];
    protected $guarded = ["id"];


    public function sprint_histories(){
         return $this->hasMany('App\Models\Sprint_History','fk_sprint_id','id');
    }
    public function User_Sprint_Tasks(){
         return $this->hasMany('App\Models\Task_Sprint_User','fk_sprint_id','id');
    }

    public function state()
    {
        return $this->belongsTo('App\Models\State','fk_state_id','id');
    }
    public function project()
    {
        return $this->belongsTo('App\Models\Project','fk_project_id','id');
    }
}
