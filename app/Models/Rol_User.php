<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rol_User extends Model
{
    protected $table ="rol_users";
    protected $fillable = ['name','description'];
    protected $guarded = ["id"];

    public function user_rol_projects(){
         return $this->hasMany('App\Models\User_Rol_Project','fk_rol_id','id');
    }
}
