<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Phase_History extends Model
{
    protected $table ="phase_histories";
    protected $fillable = ['name'];
    protected $guarded = ["id"];

    public function histories(){
         return $this->hasMany('App\Models\History','fk_phase_id','id');
    }
}
