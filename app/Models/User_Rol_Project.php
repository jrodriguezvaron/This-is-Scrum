<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User_Rol_Project extends Model
{
	protected $table ="user_rol_projects";
    protected $fillable = ['fk_user_id','fk_rol_id','fk_project_id'];
    protected $guarded = ["id"];

    public function user()
    {
        return $this->belongsTo('App\User','fk_user_id','id');
    }

    public function rol()
    {
        return $this->belongsTo('App\Models\Rol_User','fk_rol_id','id');
    }

    public function project()
    {
        return $this->belongsTo('App\Models\Project','fk_project_id','id');
    }


    public function User_Sprint_Tasks(){
         return $this->hasMany('App\Models\Task_Sprint_User','fk_user_id','id');
    }
}
