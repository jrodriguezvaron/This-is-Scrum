<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Histori_Sprint extends Model
{
    protected $table ="history_sprint";
    protected $fillable = ['history_id','sprint_id'];
    protected $guarded = ["id"];


    public function history()
    {
        return $this->belongsTo('App\Models\History','history_id','id');
    }
    
    public function sprint(){
        return $this->belongsTo('App\Models\Priority_History','sprint_id','id');
    }
}

