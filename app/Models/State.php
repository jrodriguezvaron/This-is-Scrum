<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
   protected $table ="states";
   protected $fillable = ['name'];
   protected $guarded = ["id"];


	public function users(){
        return $this->hasMany('App\User','fk_state_id', 'id');
    }
    public function projects(){
        return $this->hasMany('App\Models\Project','fk_state_id', 'id');
    }
    public function sprints(){
        return $this->hasMany('App\User\Sprint','fk_state_id', 'id');
    }
   
}
