<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Backlog extends Model
{
    protected $table ="backlogs";
    protected $fillable = ['name','description','file','fk_project_id','fk_type_id','fk_priority_id'];
    protected $guarded = ["id"];


    public function project()
    {
        return $this->belongsTo('App\Models\Project','fk_project_id','id');
    }

    public function type(){
        return $this->belongsTo('App\Models\Type_Request','fk_type_id','id');
    }

    public function priority()
    {
        return $this->belongsTo('App\Models\Priority_Request','fk_priority_id','id');
    }

    public function histories()
    {
        return $this->belongsToMany('App\Models\History')->withTimestamps();
    }
}
