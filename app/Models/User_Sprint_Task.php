<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class User_Sprint_Task extends Model
{
    protected $table ="histories";
    protected $fillable = ['id','user_id','sprint_id','task_id'];

    public function user()
    {
        return $this->belongsTo('App\Models\User_Rol_Project');
    }

    public function sprint()
    {
        return $this->belongsTo('App\Models\Sprint');
    }

    public function task()
    {
        return $this->belongsTo('App\Models\Task'');
    }

}
