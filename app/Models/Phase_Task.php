<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Phase_Task extends Model
{
    protected $table ="phase_tasks";
    protected $fillable = ['name'];
    protected $guarded = ["id"];

    public function tasks(){
         return $this->hasMany('App\Models\Task','fk_phase_id','id');
    }
}
