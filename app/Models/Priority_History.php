<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Priority_History extends Model
{
    protected $table ="priority_histories";
    protected $fillable = ['name'];
    protected $guarded = ["id"];


    public function histories(){
         return $this->hasMany('App\Models\History','fk_priority_id','id');
    }
}
