<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $table ="tasks";
    protected $fillable = ['name','description','file','fk_phase_id','fk_priority_id','fk_project_id'];
    protected $guarded = ["id"];

    public function User_Sprint_Tasks(){
         return $this->hasMany('App\Models\Task_Sprint_User','fk_task_id','id');
    }

    public function project()
    {
        return $this->belongsTo('App\Models\Project','fk_project_id','id');
    }

    public function phase_task()
    {
        return $this->belongsTo('App\Models\Phase_Task','fk_phase_id','id');
    }

    public function priority_task()
    {
        return $this->belongsTo('App\Models\Priority_Task','fk_priority_id','id');
    }
}
