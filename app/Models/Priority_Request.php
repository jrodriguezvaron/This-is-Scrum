<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Priority_Request extends Model
{
    protected $table ="priority_requests";
    protected $fillable = ['name'];
    protected $guarded = ["id"];


    public function backlogs(){
         return $this->hasMany('App\Models\Backlog','fk_priority_id','id');
    }
}
