<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $table ="projects";
   	protected $fillable = ['name','description','fk_state_id'];
    protected $guarded = ["id"];


    public function state()
    {
        return $this->belongsTo('App\Models\State','fk_state_id','id');
    }

    public function user_rol_projects(){
         return $this->hasMany('App\Models\User_Rol_Project','fk_project_id','id');
    }

    public function backlogs(){
         return $this->hasMany('App\Models\Backlog','fk_project_id','id');
    }
}
