<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
	Route::group(['prefix' => 'Administrador'], function(){
	
		Route::resource('User', 'Auth\UserController');
		Route::resource('Backlog', 'Backlog\BacklogController');
		Route::resource('Backlog_History', 'Backlog_history\Backlog_historyController');
		Route::resource('History', 'History\HistoryController');
		Route::resource('Project', 'Project\ProjectController');
		Route::resource('Sprint', 'Sprint\SprintController');
		Route::resource('Sprint_History', 'Sprint_history\Sprint_historyController');
		Route::resource('Task', 'Task\TaskController');
		Route::resource('Task_Sprint_User', 'Task_sprint_user\Task_sprint_userController');
		Route::resource('User_Rol_Project', 'User_Rol_Project\User_Rol_ProjectController');

		
});

		


Route::get('Administrador/Backlogs/{Backlogs}',[
			'as'	=>	'Backlogs.shows',
			'uses'	=>	'Backlog\BacklogController@index_show'
		]);

Route::get('Administrador/Historys/{Historys}',[
			'as'	=>	'Historys.shows',
			'uses'	=>	'History\HistoryController@index_show'
		]);

Route::get('Administrador/Tasks/{Tasks}',[
			'as'	=>	'Tasks.shows',
			'uses'	=>	'Task\TaskController@index_show'
		]);

Route::get('Administrador/Projects/{Projects}',[
			'as'	=>	'Projects.shows',
			'uses'	=>	'Project\ProjectController@index_show'
		]);

	
	 
Auth::routes();

Route::get('/home', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index');

Auth::routes();

Route::get('/home', 'HomeController@index');
