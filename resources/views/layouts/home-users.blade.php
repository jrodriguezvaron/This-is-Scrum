<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title> Scrum SENA | @yield('title','Inicio')</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="{{asset('complementos/css/bootstrap.min.css')}}">
    <!-- Google fonts - Roboto -->
    <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="{{asset('complementos/css/style.default.css')}}" id="theme-stylesheet">
    <!-- jQuery Circle-->
    <link rel="stylesheet" href="{{asset('complementos/css/grasp_mobile_progress_circle-1.0.0.min.css')}}">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="{{asset('complementos/css/custom.css')}}">
    <!-- Favicon-->
    <link rel="shortcut icon" href="{{asset('complementos/img/faviconSena.ico')}}">
    <!-- Font Awesome CDN-->
    <!-- you can replace it by local Font Awesome-->
    <link rel="stylesheet"  href="{{asset('plugins/font-awesome-4.7.0/css/font-awesome.min.css')}}">
    </head>
  <body>
    <!-- Side Navbar -->
    <nav class="side-navbar shrink">
      <div class="side-navbar-wrapper">
        <div class="sidenav-header d-flex align-items-center justify-content-center">
          <div class="sidenav-header-inner text-center"><img src="{{asset('/img/Logoo.png')}}" alt="person" class="img-fluid rounded-circle">
            <h2 class="h5 text-uppercase">SS</h2><span class="text-uppercase">Scrum Sena</span>
          </div>
          <div class="sidenav-header-logo"><a href="/home" class="brand-small text-center"> <strong class="text-primary">S</strong><strong class="text-primary">S</strong></a></div>
        </div>
        <div class="main-menu">
          <ul id="side-main-menu" class="side-menu list-unstyled">                  
            <li class=""><a href="{{route('Projects.shows', Auth::user())}}"> <i class="fa fa-tasks" aria-hidden="true"></i><span>Proyectos</span></a></li>
            @if(Auth::user()->admin())
            <li> <a href="{{ route('User.index')}}"><i class="fa fa-users" aria-hidden="true"></i><span>Usuarios</span></a></li>
            @endif
            <li>
                <a  href="{{ url('/logout') }}" 
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                    
                        <i class="fa fa-sign-out"></i><span>Cerrar Sesion </span> 
                    
                </a>
                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                     {{ csrf_field() }}
                </form>
            </li>
          </ul>
        </div>
        
      </div>
    </nav>
    <div class="page home-page active">
      <!-- navbar-->
      <header class="header">
        <nav class="navbar">
          <div class="container-fluid">
            <div class="navbar-holder d-flex align-items-center justify-content-between">
              <div class="navbar-header"><a id="toggle-btn" href="/home" class="menu-btn"><i class="fa fa-sign-in fa-2x" aria-hidden="true"> </i></a><a href="/home" class="navbar-brand">
                  <div class="brand-text hidden-sm-down"><span>SCRUM </span><strong class="text-primary">SENA</strong></div></a>
              </div>
              <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center">
                @if(Auth::user()->admin())
                <li class="nav-item dropdown"> <a id="notifications" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link"><i class="fa fa-group"></i></a>
                  <ul aria-labelledby="notifications" class="dropdown-menu">
                    <li><a rel="nofollow" href="{{ route('User.index')}}" class="dropdown-item"> 
                        <div class="notification d-flex justify-content-between">
                          
                          <div class="notification-content"><i class="fa fa-group"></i>Gestion Usuarios </div>
                          

                        </div></a></li>
                  </ul>               
                </li>@endif
                <li class="nav-item dropdown"> <a id="notifications" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link"><i class="fa fa-laptop"></i></a>
                  <ul aria-labelledby="notifications" class="dropdown-menu">
                    <li><a rel="nofollow" href="{{route('Projects.shows', Auth::user())}}" class="dropdown-item"> 
                        <div class="notification d-flex justify-content-between">
                          <div class="notification-content"><i class="fa fa-laptop"></i>Gestion Proyectos </div>
                        </div></a></li>
                  </ul>               
                </li>
                @if (Auth::guest())
                            <li><a href="{{ url('/login') }}">Login</a></li>
                            <li><a href="{{ url('/register') }}">Register</a></li>
                        @else
                <li class="nav-item dropdown"> <a id="notifications" rel="nofollow" data-target="#" href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="nav-link"><img src="{{asset('img/Logoo.png')}}" class="img-circle img-responsiv" alt="Cinque Terre" width="40" height="40"><span></span> {{ Auth::user()->name }} {{ Auth::user()->last_name }}<i class="fa fa-chevron-down"></i></a>
                  <ul aria-labelledby="notifications" class="dropdown-menu">
                    <li><a rel="nofollow" href="#" class="dropdown-item"> 
                        <div class="notification d-flex justify-content-between">
                          <div class="notification-content"><i class="fa fa-user"></i>Perfil </div>
                          
                        </div></a></li>
                        <li><a rel="nofollow" href="#" class="dropdown-item"> 
                        <div class="notification d-flex justify-content-between">
                          <div class="notification-content"><i class="fa fa-gear"></i>Ajustes </div>
                          
                        </div></a></li>

                        <li>
                            <a rel="nofollow" class="dropdown-item" href="{{ url('/logout') }}" 
                                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                <div class="notification d-flex justify-content-between">
                                  <div class="notification-content"><i class="fa fa-sign-out"></i>Cerrar Sesion </div>
                                </div>
                            </a>
                              <form id="logout-form" action="{{ url('/logout') }}" method="POST" ">
                                    {{ csrf_field() }}
                              </form>                          
                        </li>
                  </ul>               
                </li>
                @endif
               
              </ul>
            </div>
          </div>
        </nav>
      </header>
      
          <div class="breadcrumb-holder">   
              <div class="container-fluid">
                <ul class="breadcrumb">
                  @yield('breadcrumbs')
                  @show
               </ul>
              </div>
            </div>
            <br>
      <div class="container-fluid">
        @yield('content')  
      </div>
      
      

   
    <!-- Javascript files-->
    <script src="{{asset('plugins/JQuery/jquery.js')}}"></script>
    <script src="{{asset('complementos/js/tether.min.js')}}"></script>
    <script src="{{asset('complementos/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('complementos/js/jquery.cookie.js')}}"> </script>
    <script src="{{asset('complementos/js/grasp_mobile_progress_circle-1.0.0.min.js')}}"></script>
    <script src="{{asset('complementos/js/jquery.nicescroll.min.js')}}"></script>
    <script src="{{asset('complementos/js/jquery.validate.min.js')}}"></script>
    
    <script src="{{asset('complementos/js/front.js')}}"></script>
    <!-- Google Analytics: change UA-XXXXX-X to be your site's ID.-->
    <!---->
    <script>
      (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
      function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
      e=o.createElement(i);r=o.getElementsByTagName(i)[0];
      e.src='//www.google-analytics.com/analytics.js';
      r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
      ga('create','UA-XXXXX-X');ga('send','pageview');



    </script>
     @yield('js')  
  </body>
</html>