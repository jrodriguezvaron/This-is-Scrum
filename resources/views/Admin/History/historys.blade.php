<nav class="side-navbar ">
      <div class="side-navbar">
        <div class="sidenav-header d-flex align-items-center justify-content-center">
          <div class="sidenav-header-inner text-center"><img src="{{asset('/img/Logoo.png')}}" alt="person" class="img-fluid rounded-circle">
            <h2 class="h5 text-uppercase">SS</h2><span class="text-uppercase">Scrum Sena</span>
          </div>
          <div class="sidenav-header-logo"><a href="/home" class="brand-small text-center"> <strong class="text-primary">S</strong><strong class="text-primary">S</strong></a></div>
        </div>
        <div class="main-menu">
          <ul id="side-main-menu" class="side-menu list-unstyled">                  
            <li> <a href="{{route('Projects.shows', Auth::user())}}"> <i class="fa fa-tasks" aria-hidden="true"></i><span>Proyectos</span></a></li>
            <li> <a href="{{route('Historys.shows', $project->id)}}"><i class="fa fa-puzzle-piece" aria-hidden="true"></i><span>Historias</span></a></li>
            <li> <a href="{{route('Backlogs.shows', $project->id)}}"> <i class="fa fa-list" aria-hidden="true"></i><span>Peticiones</span></a></li>
            <li> <a data-toggle="modal" data-target="#myModal"> <i class="fa fa-comments" aria-hidden="true"> </i><span>Chat </span></a></li>
            <li> <a data-toggle="modal" data-target="#myModal"> <i class="fa fa-line-chart" aria-hidden="true"></i><span>Linea de tiempo</span></a></li>
            <li> <a data-toggle="modal" data-target="#myModal"> <i class="fa fa-cogs" aria-hidden="true"></i><span>Proyecto</span></a></li>

            <li>
                <a  href="{{ url('/logout') }}" 
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                    
                        <i class="fa fa-sign-out"></i><span>Cerrar Sesion </span> 
                    
                </a>
                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                     {{ csrf_field() }}
                </form>
            </li>
          </ul>
        </div>
        
      </div>
    </nav>

@extends('layouts.history')
{{ $contador = 1}}
@section('title',  $project->name )

@section('idproject', $project->id)

@section('project', $project->name )

@section('content')
<!--HISTORIAS-->
<div class="col-lg-12">
    <div class="row">
        <div class="col-md-9">
            <div class="card">
                <div class="card-header d-flex align-items-center">
                    <h3 class="panel-title"><i class="fa fa-list"></i>  Mis historias</h3>
                </div>
                <div class="card-block">
                    <div class="panel-body">
                        @if(count($errors) >0)
                        <div class="alert alert-danger" role="alert">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        @include('flash::message')
                        <div class="table-responsive">
                            <table class="table table-hover table-sm">
                                <thead>
                                    <tr>
                                        <th>Identificador</th>
                                        <th>Obetivo</th>
                                        <th>Fase</th>
                                        <th>Peticiones a realizar durante la historia</th>
                                        <th>Prioridad</th>
                                        <th>Acciones</th>
                                    </tr>
                                </thead>
                                <tbody> 
                                    @foreach ($history as $history)
                                    <tr id="f{{ $history -> id }}">
                                        <td>
                                            <center>#{{ $contador++}}</center>
                                        </td>
                                        <td>{{ $history -> objective}}</td>
                                        <td >{{ $history -> phase_history -> name}}</td> 
                                        <td > 
                                            @foreach ($history->backlogs as $log) 
                                         
                                         -{{$log->name}}

                                            @endforeach
                                        </td>
                                        <td>{{ $history -> priority_history  -> name}}</td>
                                        <td>                                             
                                            <a href="{{route('Tasks.shows', $project->id)}}"><i class="fa fa-fw fa-paper-plane fa-2X "></i></a>
                                            <a href="#">
                                                <i class="fa fa-fw fa-pencil fa-2x buttonAct" data-id="{{ $history -> id }}" data-toggle="modal" data-target="#Actualizar"></i>
                                            </a>
                                        </td>
                                    </tr>
                                    @endforeach           
                                </tbody>
                            </table>
                            <a><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#Crear">Nueva <i class="fa fa-fw fa-plus"></i></button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!--Sprints-->
        <div class="col-md-3">
            <div class="card">
                <div class="card-header d-flex align-items-center">
                    <h3 class="panel-title"><i class="fa fa-list"></i>Sprints</h3>
                </div>
                <div class="card-block">
                    <div class="panel-body">
                        @if(count($errors) >0)
                        <div class="alert alert-danger" role="alert">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{$error}}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        @include('flash::message')
                        <div class="table-responsive">
                            <div class="panel-heading">
                                    <div class="col-md-5">
                                        <i class="fa fa-tasks fa-5x"></i>
                                    </div>
                                    <div class="col-md-7">
                                        <div class="huge">12</div>
                                        <div>Nuevo Sprint!</div>
                                    </div>
                                

                                </div>
                            <a href="{{ url('Administrador/Sprints/create')}}">
                                    <div class="panel-footer">
                                        <span class="pull-right ">
                                            <i class="fa fa-plus fa-2x"></i>
                                        </span>
                                        <span class="pull-left">Agregar Sprint</span>
                                        
                                    </div>
                                </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>

    <div id="Crear" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Registro de historias</h4>
                </div>
                <div class="modal-body">
                    {!! Form::open(['route' => 'History.store', 'method' => 'POST']) !!}
                    <div class="form-group">
                        {!! Form::label('objective', 'Objetivo') !!}
                        {!! Form::textarea('objective', null, ['class' =>'form-control', 'requerid', 'placeholder' => 'Objetivo de la historia']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('profiles', 'Fase',[ 'class' => 'col-md-4 control-label']) !!}
                        {!! Form::select('fk_phase_id', $phase, null, ['class' => 'form-control type-select ','placeholder' => 'Seleccione alguna fase', 'required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('fk_priority_id', 'Prioridad',[ 'class' => 'col-md-4 control-label']) !!}
                        {!! Form::select('fk_priority_id', $priority, null, ['class' => 'form-control priority-select ','placeholder' => 'Seleccione alguna prioridad', 'required']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('fk_priority_id', 'Requisitos de la historia',[ 'class' => ' control-label']) !!}
                        {!! Form::select('backlog[]', $backlog, null, ['class' => 'form-control backlog-select ','placeholder' => 'Seleccione alguna prioridad', 'required','multiple']) !!}
                    </div>
                    <div >
                        {!! Form::hidden('fk_project_id', $project->id)!!}
                    </div>
                    <div class="modal-footer">
                        {!! Form::submit('Registrar', ['class' =>'btn btn-success col-md-offset-5']) !!}
                    </div>

                    {!! Form::close() !!}
                </div>

            </div>
        </div>
    </div>

    <div id="Actualizar" class="modal fade" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Actualizar Usuario</h4>
                </div>
                <div class="modal-body">

                    {!! Form::open(['route' =>  ['History.update', ':ID'], 'method' => 'PUT',  'id' => 'formUpdate']) !!}
                    <div class="form-group">
                        {!! Form::label('name', 'Objetivo') !!}
                        {!! Form::text('objective', null, ['class' =>'form-control', 'requerid','id' => 'obje']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('profiles', 'Fase',[ 'class' => 'col-md-4 control-label']) !!}
                        {!! Form::select('fk_phase_id', $phase, null, ['class' => 'form-control type-select ','placeholder' => 'Seleccione alguna fase', 'required','id' => 'fase']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('fk_priority_id', 'Prioridad',[ 'class' => 'col-md-4 control-label']) !!}
                        {!! Form::select('fk_priority_id', $priority, null, ['class' => 'form-control priority-select ','placeholder' => 'Seleccione alguna prioridad', 'required','id' => 'prio']) !!}
                    </div>
                    <div class="form-group">
                        {!! Form::label('fk_priority_id', 'Requisitos de la historia',[ 'class' => ' control-label']) !!}
                        {!! Form::select('backlog[]', $backlog, null, ['class' => 'form-control backlogs-select ','placeholder' => 'Seleccione alguna prioridad', 'required','multiple']) !!}
                    </div>
                    <div >
                        {!! Form::hidden('fk_project_id', $project->id)!!}
                    </div>
                    <div class="modal-footer">
                        {!! Form::submit('Actualizar', ['class' =>'btn btn-info col-md-offset-5']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>

            </div>
        </div>
    </div>
</div>



<div class="container">
    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Oooooops</h4>
                </div>
                <div class="modal-body">
                    <img src="{{asset('img/trabajando.jpg')}}" class="img-rounded" alt="Cinque Terre" width="780" height="347">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">cerrar</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection



@section('js')
<script>
    $('#Crear').on('shown.bs.modal', function () {
        $('.backlog-select', this).chosen({
            placeholder_text_single: "seleccione algo",
        });
    });
    $('#Actualizar').on('shown.bs.modal', function () {
        $('.backlogs-select', this).chosen({

        });
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $(".buttonAct").on("click", function () {
            var id = $(this).attr("data-id");
            var obj = $("#f" + id + " td:nth-child(2)").text();
            var phase = $("#f" + id + " td:nth-child(3)").text();
            var priority = $("#f" + id + " td:nth-child(4)").text();
            var requisi = $("#f" + id + " td:nth-child(5)").text();
            $("#obje").val(obj);
            $("#fase").val(phase);
            $("#prio").val(priority);
            $("#requi").val(requisi);
            var form = $("#formUpdate");
            var url = form.attr('action').replace(':ID', id);
            form.attr('action', url);
        });
    });
</script>  
@endsection
