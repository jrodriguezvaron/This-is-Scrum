
@extends('layouts.home-users')
{{ $contador = 1}}
@section('title', 'Proyectos')

@section('breadcrumbs')
<center><div class="col-lg-4">
    
    <div class="input-group">
      
      <input type="text" class="form-control" placeholder="Ingrese el nombre del proyecto">
      
      <span class="input-group-btn">
        <button class="btn btn-success" type="button">Go!</button>
      </span>

      
    </div><!-- /input-group -->
  </div><!-- /.col-lg-6 --></center>
@endsection

@section('content')
<div class="row">
    <div class="col-md-8">
<button class="btn btn-primary" data-toggle="modal" data-target="#Crear"><span>Crear Proyecto</span><i class="fa fa-plus"></i></button>

    
    
    <br><br>

    @include('flash::message')
    <div class="row">
    @foreach ($user_project as $user_project)
    
        <div class="col-lg-4">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h3 class="panel-title "><i class="fa fa-dashboard"></i> Proyecto #{{$contador ++}}</h3>
                </div>
                <div class="card-block">
                @if(count($errors) >0)
                <div class="alert alert-danger" role="alert">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                <tr id="f{{ $user_project -> id }}">
                <form>
                    <div class="form-control">
                    <div class="col-md-12 form-group text-center">
                        <img src="">
                      </div>
                      <div class="col-md-12 form-group text-center">
                      <h2 class="p-1"><strong>{{$user_project->project->name}}</strong></h2>
                      <hr>                
                      </div>
                      <div class="col-md-12 form-group">
                          <div class="col-md-12">
                            <label><strong>Descripcion</strong></label>                
                          </div>
                          <div class="col-md-12">
                            <p class="text-paragraph text-email"><strong>{{$user_project->project->description}}</strong></p>                
                          </div>
                      </div>
                      <div class="col-md-12 form-group">
                          <div class="col-md-12">
                            <label><strong>Rol</strong></label>                
                          </div>
                          <div class="col-md-12">
                            <p class="text-paragraph text-email"><strong>{{$user_project->rol->name}}</strong></p>                
                          </div>
                      </div>
                     
                    <div class="form-group row">       
                      <div class="col-sm-10 offset-sm-2">
                        <td>                                            
                        <a href="{{route('Backlogs.shows', $user_project->project->id)}}"><i class ="fa fa-fw fa-paper-plane fa-2X "></i></a>
                        </td>
                        <td>
                        <a href ="#"><i class="fa fa-fw fa-pencil fa-2x buttonAct" data-id="{{ $user_project -> id }}" data-toggle="modal" data-target="#Actualizar"></i></a>
                        </td>   
                      </div>
                    </div>
                    </div>
                  </form>
                  
                </div>
              </div>
            </div>
    
            @endforeach 
            </div>
            </div>



<div class ="col-md-4">
    <div class="card">
        <div class="card-header d-flex align-items-center">
            <h3 class="panel-title">Roles de usuario</h3>
        </div>
        <div class="card-block"> 
            <div class="panel-body">
                @if(count($errors) >0)
                <div class="alert alert-danger" role="alert">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                @include('flash::message')
                <div class="table-responsive">
                    <table class="table table-hover table-sm">
                        <thead>
                            <tr>
                                <th></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>            
                            @foreach ($rol as $rol)
                            <tr>
                                <tr><th>{{$rol->name}}:</th></tr>
                                <tr><th>{{$rol->description}}</th></tr>
                                
                            </tr>
                            @endforeach 
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
</div>






<div id="Crear" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Registrar Proyecto</h4>
            </div>
            <div class="modal-body">

                {!! Form::open(['route' => 'Project.store', 'method' => 'POST']) !!}

                <div class="form-group">
                    {!! Form::label('name', 'Nombre') !!}
                    {!! Form::text('name', null, ['class' =>'form-control', 'requerid', 'placeholder' => 'Nombre del proyecto']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('name', 'Descripcion') !!}
                    {!! Form::textarea('description', null, ['class' =>'form-control textarea-description', 'requerid', 'placeholder' => 'Descripcion del proyecto']) !!}
                </div>
                
                    {!! Form::hidden('algo', Auth::user()->id, ['class' =>'form-control textarea-description']) !!}

                <div class="modal-footer">
                    {!! Form::submit('Registrar', ['class' =>'btn btn-success col-md-offset-5']) !!}
                </div>

                {!! Form::close() !!}

            </div>
        </div>
    </div>
</div>




<!--Ventana Modal-->
<div id="Actualizar" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Actualizar Proyecto</h4>
            </div>
            <div class="modal-body">

                {!! Form::open(['route' =>  ['Project.update', ':ID'], 'method' => 'PUT',  'id' => 'formUpdate']) !!}

                <div class="form-group">
                    {!! Form::label('name', 'Nombre') !!}
                    {!! Form::text('name', null, ['class' =>'form-control', 'requerid', 'placeholder' => 'Nombre del proyecto','id' => 'nombre']) !!}
                </div>

                <div class="form-group">
                    {!! Form::label('name', 'Descripcion') !!}
                    {!! Form::textarea('description', null, ['class' =>'form-control textarea-description', 'requerid', 'placeholder' => 'Descripcion del proyecto','id' => 'descri']) !!}
                </div>                                        

                <div class="modal-footer">
                    {!! Form::submit('Actualizar', ['class' =>'btn btn-info col-md-offset-5']) !!}
                </div>
                {!! Form::close() !!} 
            </div>

        </div>
    </div>
</div>


</div>
</div>
</div>

@endsection

@section('js')
<script type="text/javascript">

    $(document).ready(function () {


        $(".buttonAct").on("click", function () {

            var id = $(this).attr("data-id");
            var name = $("#f" + id + " td:nth-child(1)").text();
            var desc = $("#f" + id + " td:nth-child(2)").text();

            $("#nombre").val(name);
            $("#descri").val(desc);

            var form = $("#formUpdate");
            var url = form.attr('action').replace(':ID', id);
            form.attr('action', url);

        });

    });


</script>   
@endsection