<nav class="side-navbar ">
      <div class="side-navbar">
        <div class="sidenav-header d-flex align-items-center justify-content-center">
          <div class="sidenav-header-inner text-center"><img src="{{asset('/img/Logoo.png')}}" alt="person" class="img-fluid rounded-circle">
            <h2 class="h5 text-uppercase">SS</h2><span class="text-uppercase">Scrum Sena</span>
          </div>
          <div class="sidenav-header-logo"><a href="/home" class="brand-small text-center"> <strong class="text-primary">S</strong><strong class="text-primary">S</strong></a></div>
        </div>
        <div class="main-menu">
          <ul id="side-main-menu" class="side-menu list-unstyled">                  
            <li> <a href="{{route('Projects.shows', Auth::user())}}"> <i class="fa fa-tasks" aria-hidden="true"></i><span>Proyectos</span></a></li>
            <li> <a href="{{route('Historys.shows', $project->id)}}"><i class="fa fa-puzzle-piece" aria-hidden="true"></i><span>Historias</span></a></li>
            <li> <a href="{{route('Backlogs.shows', $project->id)}}"> <i class="fa fa-list" aria-hidden="true"></i><span>Peticiones</span></a></li>
            <li> <a data-toggle="modal" data-target="#myModal"> <i class="fa fa-comments" aria-hidden="true"> </i><span>Chat </span></a></li>
            <li> <a data-toggle="modal" data-target="#myModal"> <i class="fa fa-line-chart" aria-hidden="true"></i><span>Linea de tiempo</span></a></li>
            <li> <a data-toggle="modal" data-target="#myModal"> <i class="fa fa-cogs" aria-hidden="true"></i><span>Proyecto</span></a></li>

            <li>
                <a  href="{{ url('/logout') }}" 
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                    
                        <i class="fa fa-sign-out"></i><span>Cerrar Sesion </span> 
                    
                </a>
                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                     {{ csrf_field() }}
                </form>
            </li>
          </ul>
        </div>
        
      </div>
    </nav>
    
@extends('layouts.app')
	{{ $contador = 1}}
@section('title',  $project->name )

@section('idproject', $project->id)

@section('project', $project->name )

@section('content')
	  <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h3 class="panel-title"><i class="fa fa-list"></i>  Mis peticiones</h3>
                </div>
                <div class="card-block">
                            <div class="panel-body">
                                @if(count($errors) >0)
                                    <div class="alert alert-danger" role="alert">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{$error}}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                            @include('flash::message')
                                <div class="table-responsive">
                                   <table class="table table-hover table-sm">
                                        <thead>
                                        <tr>
                                            <th>Identificador</th>
                                            <th>Nombre</th>
                                            <th>Descripcion</th>
                                            <th>Tipo de peticion</th>
                                            <th>Prioridad</th>
                                            <th>Editar</th>
                                        </tr>
                                        </thead>
                                        <tbody>            
                                            @foreach ($backlogs as $backlogs)
                                        <tr id="f{{ $backlogs -> id }}">
                                            <td>
                                                {{ $contador++}}
                                            </td>
                                                <td>{{ $backlogs -> name}}</td>
                                                <td>{{ $backlogs -> description}}</td>
                                                <td >{{ $backlogs -> type->name }}</td>
                                                <td>{{ $backlogs -> priority->name}}</td>
                                                <td>
                                                <center>
                                                    <a href="#">
                                                        <i class="fa fa-fw fa-pencil fa-2x buttonAct" data-id="{{ $backlogs->id }}" data-toggle="modal" data-target="#Actualizar"></i>
                                                    </a>
                                                </center>
                                            </td>                            
                                        </tr>
                                            @endforeach 
                                        </tbody>
                                    </table>
                                    <a ><button type="button" class="btn btn-primary" data-toggle="modal"   data-target="#Crear">Nueva <i class="fa fa-fw fa-plus"></i></button></a><br><br>
                                </div>
                            </div>
                    </div>
                </div>



                <div id="Crear" class="modal fade" role="dialog">
                                <div class="modal-dialog">

                                        <!-- Modal content-->
                                    <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Registro de Peticion</h4>
                                          </div>
                                          <div class="modal-body">


                                        {!! Form::open(['route' => 'Backlog.store', 'method' => 'POST']) !!}

                                        <div class="form-group">
                                            {!! Form::label('name', 'Nombre') !!}
                                            {!! Form::text('name', null, ['class' =>'form-control', 'requerid', 'placeholder' => 'Nombre de la peticion']) !!}
                                        </div>
                                        
                                        <div class="form-group">
                                            {!! Form::label('name', 'Descripcion') !!}
                                            {!! Form::textarea('description', null, ['class' =>'form-control textarea-description', 'requerid', 'placeholder' => 'Descripcion de la peticion']) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::hidden('fk_project_id', $project->id, ['class' =>'']) !!}
                                        </div>
                                        
                                        <div class="form-group">
                                            {!! Form::label('profiles', 'Tipo de requisito',[ 'class' => 'col-md-4 control-label']) !!}
                                            {!! Form::select('fk_type_id', $type, null, ['class' => 'form-control type-select ', 'required']) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('fk_priority_id', 'Prioridad',[ 'class' => 'col-md-4 control-label']) !!}
                                            {!! Form::select('fk_priority_id', $priority, null, ['class' => 'form-control priority-select ', 'required']) !!}
                                        </div>



                                        <div class="modal-footer">
                                            {!! Form::submit('Registrar', ['class' =>'btn btn-success col-md-offset-5']) !!}
                                        </div>

                                        {!! Form::close() !!}
                                        </div>
                                          
                                    </div>
                                </div>
                            </div> 



                             <div id="Actualizar" class="modal fade" role="dialog">
                                <div class="modal-dialog">

                                        <!-- Modal content-->
                                    <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Actualizar Peticion</h4>
                                          </div>
                                    <div class="modal-body">

                                    {!! Form::open(['route' =>  ['Backlog.update', ':ID'], 'method' => 'PUT',  'id' => 'formUpdate']) !!}

                                         
                                        <div class="form-group">
                                            {!! Form::hidden('fk_project_id', $project->id, ['class' =>'']) !!}
                                        </div>
                                         <div class="form-group">
                                            {!! Form::label('name', 'Nombre') !!}
                                            {!! Form::text('name', null, ['class' =>'form-control', 'requerid', 'placeholder' => 'Nombre del proyecto','id' => 'nombre']) !!}
                                        </div>
                                        
                                        <div class="form-group">
                                            {!! Form::label('name', 'Descripcion') !!}
                                            {!! Form::textarea('description', null, ['class' =>'form-control textarea-description', 'requerid', 'placeholder' => 'Descripcion del proyecto','id' => 'descri']) !!}
                                        </div>

                                        <div class="form-group">
                                            {!! Form::label('profiles', 'Tipo de requisito',[ 'class' => 'col-md-4 control-label']) !!}
                                            {!! Form::select('fk_type_id', $type, null, ['class' => 'form-control type-select ', 'required','placeholder' => 'Selecione tipo de requisito','id' => 'tipo']) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('fk_priority_id', 'Prioridad',[ 'class' => 'col-md-4 control-label']) !!}
                                            {!! Form::select('fk_priority_id', $priority, null, ['class' => 'form-control priority-select ', 'placeholder' => 'Seleccione la prioridad', 'required','id' => 'prio']) !!}
                                        </div>                                  
                                                                           
                                        <div class="modal-footer">
                                          {!! Form::submit('Actualizar', ['class' =>'btn btn-info col-md-offset-5']) !!}
                                        </div>
                                    {!! Form::close() !!} 
                                    </div>
                                          
                                    </div>
                                </div>
                            </div>


            </div>



<div class="container">
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Oooooops</h4>
        </div>
        <div class="modal-body">
          <img src="{{asset('img/trabajando.jpg')}}" class="img-rounded" alt="Cinque Terre" width="780" height="347">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">cerrar</button>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection

@section('js')<!--
<script>
	$(".type-select").chosen({
		placeholder_text_single: "seleccione algo",
	}); 

	$(".priority-select").chosen({
		allow_single_deselect: true,
	}); 
</script>   -->
<script type="text/javascript">
    
    $(document).ready(function(){


$(".buttonAct").on("click",function(){

var id = $(this).attr("data-id");
var name = $("#f"+id+" td:nth-child(2)").text();
var desc = $("#f"+id+" td:nth-child(3)").text();
var types = $("#f"+id+" td:nth-child(4)").text();
var prior = $("#f"+id+" td:nth-child(5)").text();
$("#nombre").val(name);
$("#descri").val(desc);
$("#tipo").val(types);
$("#prio").val(prior);

var form = $("#formUpdate");
var url = form.attr('action').replace(':ID', id);
form.attr('action', url);

});

    });


</script> 
@endsection
