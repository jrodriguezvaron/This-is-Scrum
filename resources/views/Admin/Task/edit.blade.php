    <nav class="side-navbar">
      <div class="side-navbar-wrapper">
        <div class="sidenav-header d-flex align-items-center justify-content-center">
          <div class="sidenav-header-inner text-center"><img src="{{asset('/img/Logoo.png')}}" alt="person" class="img-fluid rounded-circle">
            <h2 class="h5 text-uppercase">SS</h2><span class="text-uppercase">Scrum Sena</span>
          </div>
          <div class="sidenav-header-logo"><a href="index.html" class="brand-small text-center"> <strong class="text-primary">S</strong><strong class="text-primary">S</strong></a></div>
        </div>
        <div class="main-menu">
          <ul id="side-main-menu" class="side-menu list-unstyled">                  
            <li> <a href="{{route('Project.index')}}"> <i class="fa fa-tasks" aria-hidden="true"></i><span>Proyectos</span></a></li>
            <li> <a href="{{route('Historys.shows', $project->id)}}"><i class="fa fa-puzzle-piece" aria-hidden="true"></i><span>Historias</span></a></li>
            <li> <a href="{{route('Backlogs.shows', $project->id)}}"> <i class="fa fa-list" aria-hidden="true"></i><span>Peticiones</span></a></li>
            <li> <a href="{{route('History.index')}}"> <i class="fa fa-comments" aria-hidden="true"> </i><span>Chat </span></a></li>
            <li> <a href="{{route('History.index')}}"> <i class="fa fa-line-chart" aria-hidden="true"></i><span>Linea de tiempo</span></a></li>
            <li> <a href="{{route('Project.index')}}"> <i class="fa fa-cogs" aria-hidden="true"></i><span>Proyecto</span></a></li>
            <li>
                <a  href="{{ url('/logout') }}" 
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                    
                        <i class="fa fa-sign-out"></i><span>Cerrar Sesion </span> 
                    
                </a>
                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                     {{ csrf_field() }}
                </form>
            </li>
          </ul>
        </div>
        
      </div>
    </nav>
    
@extends('layouts.task')
    {{ $contador = 1}}
@section('title',  $project->name )

@section('idproject', $project->id)

@section('project', $project->name )

@section('content')
    <div class="container">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                                <h3 class="panel-title">Modificar tarea: {{$tasks->name}}</h3>

                            <div class="panel-body">
                                @if(count($errors) >0)
                                    <div class="alert alert-danger" role="alert">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{$error}}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                            @include('flash::message')
                                <div class="table-responsive">
                                   {!! Form::open(['route' => ['Task.update', $tasks->id], 'method' => 'PUT']) !!}

                                        <div class="form-group">
                                            {!! Form::label('name', 'Nombre') !!}
                                            {!! Form::text('name', $tasks->name, ['class' =>'form-control', 'requerid', 'placeholder' => 'Nombre de la tarea']) !!}
                                        </div>
                                        
                                        <div class="form-group">
                                            {!! Form::label('name', 'Descripcion') !!}
                                            {!! Form::textarea('description', $tasks->description, ['class' =>'form-control textarea-description', 'requerid', 'placeholder' => 'Descripcion de la peticion']) !!}
                                        </div>

                                        <div class="form-group">
                                            {!! Form::hidden('fk_project_id', $tasks->fk_project_id, ['class' =>'']) !!}
                                        </div>
                                        
                                        <div class="form-group">
                                            {!! Form::label('profiles', 'Tipo de requisito',[ 'class' => 'col-md-4 control-label']) !!}
                                            {!! Form::select('fk_phase_id', $type, $tasks->phase_task, ['class' => 'form-control type-select ', 'required']) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('fk_priority_id', 'Prioridad',[ 'class' => 'col-md-4 control-label']) !!}
                                            {!! Form::select('fk_priority_id', $priority, $tasks->priority_task, ['class' => 'form-control priority-select ', 'required']) !!}
                                        </div>                                 
                                                                           
                                        <div class="modal-footer">
                                          {!! Form::submit('Actualizar', ['class' =>'btn btn-info col-md-offset-5']) !!}
                                        </div>
                                    {!! Form::close() !!} 
                                </div>
                            </div>
                    </div>
                </div>

                

@endsection

@section('js')
<script>/*
    $('#Crear').on('shown.bs.modal', function () {
        $('.backlog-select', this).chosen({
            placeholder_text_single: "seleccione algo",
        });
    });*/
</script>
@endsection