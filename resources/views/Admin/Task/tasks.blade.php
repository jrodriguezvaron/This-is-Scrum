<nav class="side-navbar ">
      <div class="side-navbar">
        <div class="sidenav-header d-flex align-items-center justify-content-center">
          <div class="sidenav-header-inner text-center"><img src="{{asset('/img/Logoo.png')}}" alt="person" class="img-fluid rounded-circle">
            <h2 class="h5 text-uppercase">SS</h2><span class="text-uppercase">Scrum Sena</span>
          </div>
          <div class="sidenav-header-logo"><a href="/home" class="brand-small text-center"> <strong class="text-primary">S</strong><strong class="text-primary">S</strong></a></div>
        </div>
        <div class="main-menu">
          <ul id="side-main-menu" class="side-menu list-unstyled">                  
            <li> <a href="{{route('Projects.shows', Auth::user())}}"> <i class="fa fa-tasks" aria-hidden="true"></i><span>Proyectos</span></a></li>
            <li> <a href="{{route('Historys.shows', $project->id)}}"><i class="fa fa-puzzle-piece" aria-hidden="true"></i><span>Historias</span></a></li>
            <li> <a href="{{route('Backlogs.shows', $project->id)}}"> <i class="fa fa-list" aria-hidden="true"></i><span>Peticiones</span></a></li>
            <li> <a data-toggle="modal" data-target="#myModal"> <i class="fa fa-comments" aria-hidden="true"> </i><span>Chat </span></a></li>
            <li> <a data-toggle="modal" data-target="#myModal"> <i class="fa fa-line-chart" aria-hidden="true"></i><span>Linea de tiempo</span></a></li>
            <li> <a data-toggle="modal" data-target="#myModal"> <i class="fa fa-cogs" aria-hidden="true"></i><span>Proyecto</span></a></li>

            <li>
                <a  href="{{ url('/logout') }}" 
                    onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                    
                        <i class="fa fa-sign-out"></i><span>Cerrar Sesion </span> 
                    
                </a>
                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                     {{ csrf_field() }}
                </form>
            </li>
          </ul>
        </div>
        
      </div>
    </nav>
    
@extends('layouts.task')
	{{ $contador = 1}}
@section('title',  $project->name )

@section('idproject', $project->id)

@section('project', $project->name )

@section('content')

</head>
<body>
    <div class="container">
  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Oooooops</h4>
        </div>
        <div class="modal-body">
          <img src="{{asset('img/trabajando.jpg')}}" class="img-rounded" alt="Cinque Terre" width="780" height="347">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">cerrar</button>
        </div>
      </div>
    </div>
  </div>
</div>


    <h1>Tablero de control<a><button type="button" class="btn btn-deafult" data-toggle="modal" data-target="#Crear">Nueva <i class="fa fa-fw fa-plus"></i></button></a></h1>
     @if(count($errors) >0)
                                    <div class="alert alert-danger" role="alert">
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{$error}}</li>
                                            @endforeach
                                        </ul>
                                    </div>
                                @endif
                            @include('flash::message')
    
    <div id="board">
        <div id="todo" class="section">
            <h1>Historias de usuario</h1>
                
                

        </div>        
        @foreach ($phase as $phase)

        <div data-id="{{$phase->id}}" id="todo" class="section">
            <h1>{{$phase->name}}</h1>

                @foreach ($phase->tasks as $tasks)
                    
                      <div id="{{$tasks->id}}" class="card">
                        <tr>
                            <td>{{$tasks->name}}</td><br>
                            <td>{{$tasks->phase_task->name}}</td><br>
                            <td>{{$tasks->priority_task->name}}</td><br>
                        </tr>
                        <a href="{{route('Task.edit', $tasks->id)}}">
                            <i class="fa fa-fw fa-pencil buttonAct"></i>
                        </a>
                    </div>        

              @endforeach

        </div>
        @endforeach
        
    </div>



<div id="Crear" class="modal fade" role="dialog">
                                <div class="modal-dialog">

                                        <!-- Modal content-->
                                    <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Registro de tareas</h4>
                                          </div>
                                          <div class="modal-body">


                                        {!! Form::open(['route' => 'Task.store', 'method' => 'POST']) !!}

                                        <div class="form-group">
                                            {!! Form::label('name', 'Nombre') !!}
                                            {!! Form::text('name', null, ['class' =>'form-control', 'requerid', 'placeholder' => 'Nombre de la tarea']) !!}
                                        </div>
                                        
                                        <div class="form-group">
                                            {!! Form::label('name', 'Descripcion') !!}
                                            {!! Form::textarea('description', null, ['class' =>'form-control textarea-description', 'requerid', 'placeholder' => 'Descripcion de la peticion']) !!}
                                        </div>

                                        <div class="form-group">
                                            {!! Form::hidden('fk_project_id', $project->id, ['class' =>'']) !!}
                                        </div>
                                        
                                        <div class="form-group">
                                            {!! Form::label('profiles', 'Tipo de requisito',[ 'class' => 'col-md-4 control-label']) !!}
                                            {!! Form::select('fk_phase_id', $type, null, ['class' => 'form-control type-select ', 'required']) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('fk_priority_id', 'Prioridad',[ 'class' => 'col-md-4 control-label']) !!}
                                            {!! Form::select('fk_priority_id', $priority, null, ['class' => 'form-control priority-select ', 'required']) !!}
                                        </div>



                                        <div class="modal-footer">
                                            {!! Form::submit('Registrar', ['class' =>'btn btn-success col-md-offset-5']) !!}
                                        </div>

                                        {!! Form::close() !!}
                                        </div>
                                          
                                    </div>
                                </div>
                            </div> 


                            <div id="Actualizar" class="modal fade" role="dialog">
                                <div class="modal-dialog">

                                        <!-- Modal content-->
                                    <div class="modal-content">
                                          <div class="modal-header">
                                            <button type="button" class="open" data-dismiss="modal">&times;</button>
                                            <h4 class="modal-title">Actualizar Peticion</h4>
                                          </div>
                                    <div class="modal-body">

                                    {!! Form::open(['route' => 'Task.store', 'method' => 'POST']) !!}

                                        <div class="form-group">
                                            {!! Form::label('name', 'Nombre') !!}
                                            {!! Form::text('name', null, ['class' =>'form-control', 'requerid', 'placeholder' => 'Nombre de la tarea']) !!}
                                        </div>
                                        
                                        <div class="form-group">
                                            {!! Form::label('name', 'Descripcion') !!}
                                            {!! Form::textarea('description', null, ['class' =>'form-control textarea-description', 'requerid', 'placeholder' => 'Descripcion de la peticion']) !!}
                                        </div>

                                        <div class="form-group">
                                            {!! Form::hidden('fk_project_id', $project->id, ['class' =>'']) !!}
                                        </div>
                                        
                                        <div class="form-group">
                                            {!! Form::label('profiles', 'Tipo de requisito',[ 'class' => 'col-md-4 control-label']) !!}
                                            {!! Form::select('fk_phase_id', $type, null, ['class' => 'form-control type-select ', 'required']) !!}
                                        </div>
                                        <div class="form-group">
                                            {!! Form::label('fk_priority_id', 'Prioridad',[ 'class' => 'col-md-4 control-label']) !!}
                                            {!! Form::select('fk_priority_id', $priority, null, ['class' => 'form-control priority-select ', 'required']) !!}
                                        </div>                                 
                                                                           
                                        <div class="modal-footer">
                                          {!! Form::submit('Actualizar', ['class' =>'btn btn-info col-md-offset-5']) !!}
                                        </div>
                                    {!! Form::close() !!} 
                                    </div>
                                          
                                    </div>
                                </div>
                            </div>

<input type="hidden" name="urlUpdate"  id="urlUpdate" value="{{route('Task.update', ['Task' => ':ID'])}}">
<input type="hidden" name="_token"  id="MyToken" value="{{ csrf_token() }}">



            </div>

            

@endsection


@section('js')
<script type="text/javascript">
    
    $(document).ready(function(){


$(".buttonAct").on("click",function(){

var id = $(this).attr("data-id");
var name = $("#f"+id+" td:nth-child(2)").text();
var desc = $("#f"+id+" td:nth-child(3)").text();
var types = $("#f"+id+" td:nth-child(4)").text();
var prior = $("#f"+id+" td:nth-child(5)").text();
$("#nombre").val(name);
$("#descri").val(desc);
$("#tipo").val(types);
$("#prio").val(prior);

var form = $("#formUpdate");
var url = form.attr('action').replace(':ID', id);
form.attr('action', url);

});

    });


</script> 


<script>
        $(document).ready(function() {
        var cards = document.querySelectorAll('.card');
        for (var i = 0, n = cards.length; i < n; i++) {
            var card = cards[i];
            card.draggable = true;
        };
        var board = document.getElementById('board');
        var hideMe;
        board.onselectstart = function(e) {
            e.preventDefault();
        }
        board.ondragstart = function(e) {
            console.log('dragstart');
            hideMe = e.target;
            e.dataTransfer.setData('card', e.target.id);
            e.dataTransfer.effectAllowed = 'move';
        };
        board.ondragend = function(e) {
            e.target.style.visibility = 'visible';
        };
        var lastEneterd;
        board.ondragenter = function(e) {
            console.log('dragenter');
            if (hideMe) {
                hideMe.style.visibility = 'hidden';
                hideMe = null;
            }
            // Save this to check in dragleave.
            lastEntered = e.target;
            var section = closestWithClass(e.target, 'section');
            // TODO: Check that it's not the original section.
            if (section) {
                section.classList.add('droppable');
                e.preventDefault(); // Not sure if these needs to be here. Maybe for IE?
                return false;
            }
        };
        board.ondragover = function(e) {
            // TODO: Check data type.
            // TODO: Check that it's not the original section.
            if (closestWithClass(e.target, 'section')) {
                e.preventDefault();
            }
        };
        board.ondragleave = function(e) {
            // FF is raising this event on text nodes so only check elements.
            if (e.target.nodeType === 1) {
                // dragleave for outer elements can trigger after dragenter for inner elements
                // so make sure we're really leaving by checking what we just entered.
                // relatedTarget is missing in WebKit: https://bugs.webkit.org/show_bug.cgi?id=66547
                var section = closestWithClass(e.target, 'section');
                if (section && !section.contains(lastEntered)) {
                    section.classList.remove('droppable');
                }
            }
            lastEntered = null; // No need to keep this around.
        };
        board.ondrop = function(e) {
            var section = closestWithClass(e.target, 'section');
            var id = e.dataTransfer.getData('card');
            if (id) {
                var card = document.getElementById(id);
                // Might be a card from another window.
                if (card) {
                    if (section !== card.parentNode) {
                        section.appendChild(card);
                        section.getAttribute("data-id");

                    //console.log(section.getAttribute("data-id")); MUESTRA EL IDENTIFICADOR DE LA COLUMNA
                    //console.log(card.getAttribute("id")); MUESTRA EL IDENTIFICADOR DE LA TAREA
                        

                     var MyUrl = $("#urlUpdate").val().replace(':ID', card.getAttribute("id"));  
                     var MyToken = $('#MyToken').val(); 
                     console.log(MyUrl);


                $.ajax({
                    headers: {'X-CSRF-TOKEN': MyToken },
                    url: MyUrl, 
                    type: 'POST',
                    data: { _method:'PUT', _token: MyToken, TaskId: card.getAttribute("id"), PhaseId: section.getAttribute("data-id") } ,
                    dataType: 'json',
                    success: function(result){

                        }});
                


                    }
                } else {
                    alert('couldn\'t find card #' + id);
                }
            }
            section.classList.remove('droppable');
            e.preventDefault();
        };
        function closestWithClass(target, className) {
            while (target) {
                if (target.nodeType === 1 &&
                    target.classList.contains(className)) {
                    return target;
                }
                target = target.parentNode;
            }
            return null;
        }
    });
        
    </script>
@endsection