@extends('layouts.home-users')
{{ $contador = 1}}
@section('title', 'Usuarios')

@section('breadcrumbs')
  <center><div class="col-lg-4">
    
    <div class="input-group">
      
      <input type="text" class="form-control" placeholder="Ingrese el nombre del usuario">
      
      <span class="input-group-btn">
        <button class="btn btn-success btn-lg" type="button"><i class="fa fa-search" aria-hidden="true"></i></button>
      </span>
      <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal"><i class="fa fa-plus" aria-hidden="true"></i></button>

      
    </div><!-- /input-group -->
  </div><!-- /.col-lg-6 --></center>
  
@endsection

@section('content')
            <div class="col-lg-12">
              <div class="card">
                <div class="card-header d-flex align-items-center">
                  <h3 class="panel-title"><i class="fa fa-group"></i>Lista de Usuarios</h3>
                </div><br>
                <div class="row">
                    @foreach ($user as $users)
                        <div class="col-md-3">
                            <form>
                                     <div class="form-control">
                                <div class="col-md-12 form-group text-center">
                                    <img src="">
                                  </div>
                                  <div class="col-md-12 form-group text-center">
                                    <img src="{{asset('img/logoSena.PNG')}}" class="img-circle img-responsiv" alt="Cinque Terre" width="140" height="140">
                                  <h2 class="p-1">{{ $users -> name}} {{ $users -> last_name}}</h2>
                                  <hr>                
                                  </div>
                                  <div class="col-md-12 form-group">
                                      <div class="col-md-12">
                                        <label>Nombre de usuario y correo</label>                
                                      </div>
                                      <div class="col-md-12">
                                        <p class="text-paragraph text-email">@ {{ $users -> name_user}} | {{ $users -> email}}</p>                
                                      </div>
                                  </div>
                                  <div class="col-md-12 form-group">
                                      <div class="col-md-12">
                                        <label>Estado y Rol</label>                
                                      </div>
                                      <div class="col-md-12">
                                        <p class="text-paragraph text-email">{{ $users -> state -> name}} | {{ $users -> rol -> name}}</p>                
                                      </div>
                                  </div>
                                 
                                <div class="form-group row">       
                                </div>
                                </div>
                  </form>

                        </div>
                    @endforeach
                </div>
                
                    
                </div>

<!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Registrar</h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="control-label">Nombre</label>

                            <div class="">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                            <label for="last_name" class="control-label">Apellido</label>

                            <div class="">
                                <input id="last_name" type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" required autofocus>

                                @if ($errors->has('last_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('name_user') ? ' has-error' : '' }}">
                            <label for="name_user" class=" control-label">Nombre de usuario</label>

                            <div class="6">
                                <input id="name_user" type="text" class="form-control" name="name_user" value="{{ old('name_user') }}" required autofocus>

                                @if ($errors->has('name_user'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name_user') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="control-label">Correo</label>

                            <div class="">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="control-label">Contraseña</label>

                            <div class="">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="control-label">Confirmar Contraseña</label>

                            <div class="">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    REGISTRARSE
                                </button>
                            </div>
                        </div>
                    </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

@endsection

@section('js')
<script type="text/javascript">
    
    $(document).ready(function(){


$(".buttonAct").on("click",function(){

var id = $(this).attr("data-id");
var nom = $("#f"+id+" td:nth-child(2)").text();
var last_name = $("#f"+id+" td:nth-child(3)").text();
var usua = $("#f"+id+" td:nth-child(4)").text();
var correo = $("#f"+id+" td:nth-child(5)").text();
var state = $("#f"+id+" td:nth-child(6)").text();
var profile = $("#f"+id+" td:nth-child(7)").text();


$("#nombre").val(nom);
$("#last").val(last_name);
$("#usu").val(usua);
$("#email").val(correo);
$("#estado").val(state);
$("#perfil").val(profile);

var form = $("#formUpdate");
var url = form.attr('action').replace(':ID', id);
form.attr('action', url);

});

    });


</script>   
@endsection